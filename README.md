# BDS group 10

[This project](http://54.93.238.98:8838/) is the front-end of the final assignment of Big Data Science (E018210A) of Timo Latruwe and Thibault Degrande.

## Goal of the project

Goal was to formulate a research or business question related to the COVID-19 pandemic, and turn a solution into a small product.

## Interpretation of the task

In the project, we gathered data on the statistical sectors in Flanders, Belgium, and checked if there is a correlation between them as well as with the numbers on reported COVID cases per municipality.
In particular, we investigated the correlation between those data sources and the mobiscore. More information on the analysis can be found in [this repository](https://gitlab.com/bds_project/BDS_group10/).

## Contents

In this gitlab project, the source code can be found that was used to for the vizualisation part of the assignment.

- _src_ contains the different pages of the tool
- _assets_ contains the data for the visualisations

## Run tool

The webtool can be locally hosted by simply running dash_app.py, provided all dependencies are installed. Alternatively, the tool can be visited [online](http://54.93.238.98:8838/).
