import json
import pandas as pd
import os
from definitions import ASSET_PATH
import plotly.express as px

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from src.homepage import homepage
from src.variables import variable_options, get_variables_pagecontent
from src.regression import get_regression_pagecontent, param_options
from src.accepted_model import get_model_pagecontent
from src.output import output_options, get_output_pagecontent
from src.covid import get_covid_page_content


# LOAD DATA
statsecs = json.load(open(os.path.join(ASSET_PATH, 'statsec_clean.geojson')))
df = pd.read_csv(os.path.join(ASSET_PATH, 'dataviz_master.csv'))


# VARIABLES PAGE
variables = html.Div([html.H3('Overview of explaining variables'),
                      html.Span('Choose your variable:'),
                      dcc.Dropdown(id='variable-picker', options=variable_options, value='avg_mobiscore',  # default mobiscore
                                   style={'width': '500px'}),
                      html.Hr(),
                      # fetch page content based on param
                      html.Span(children=get_variables_pagecontent(
                          'avg_mobiscore'), id='param-view')
                      ],
                     style={'width': '100%', 'float': 'left', 'paddingTop': 20, 'paddingLeft': 300})


# REGRESSION PAGE
regression = html.Div([
    html.H3('Overview of individual regression output'),
    html.Span('Choose your variable:'),
    dcc.Dropdown(id='regression-picker', options=param_options, value='pop_density_log',  # default pop density
                 style={'width': '500px'}),
    html.Hr(),
    html.Span(children=get_regression_pagecontent(
        'pop_density_log'), id='regress-view')
],
    style={'width': '100%', 'float': 'left',
           'paddingTop': 20, 'paddingLeft': 300}
)

# ACCEPTED MODEL PAGE
model = html.Div([
    html.H3('Regression output: accepted model'),
    html.Span(children=get_model_pagecontent(), id='model-view')
],
    style={'width': '100%', 'float': 'left',
           'paddingTop': 20, 'paddingLeft': 300}
)

# OUTPUT VISUAL PAGE
output = html.Div([
    html.H3('Regression output: chloropleth and error plots'),
    dcc.Dropdown(id='output-picker', options=output_options, value='error',  # default mobiscore
                 style={'width': '500px'}),
    html.Hr(),
    # fetch page content based on param
    html.Span(children=get_output_pagecontent('error'), id='outp-view')
],
    style={'width': '100%', 'float': 'left',
           'paddingTop': 20, 'paddingLeft': 300}
)

# COVID PAGE
covid = html.Div([
    html.H3('Findings on COVID-19'),
    get_covid_page_content()
],
    style={'width': '100%', 'float': 'left',
           'paddingTop': 20, 'paddingLeft': 300}
)


# CREATE APP
app = dash.Dash(external_stylesheets=[
                dbc.themes.LUX], suppress_callback_exceptions=True)  # eager_loading=True


# LAYOUT PAGE STRUCTURE
app.layout = html.Div([
    dcc.Location(id="url"),
    html.Div([
        html.H3("BDS", style={'font': 'bold', 'padding-left': '20px'}),
        html.P(
            "Group 10", style={'padding-left': '20px'}),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/", id="home",
                            className='menu-item'),
                dbc.NavLink("Input variables", href="/variables",
                            id="variables", className='menu-item'),
                dbc.NavLink("Individual regressions", href="/regression",
                            id="regression", className='menu-item'),
                dbc.NavLink("Accepted model", href="/model",
                            id="model", className='menu-item'),
                dbc.NavLink("Model results", href="/output",
                            id="output", className='menu-item'),
                dbc.NavLink("COVID-19 vs Mobiscore", href="/covid",
                            id="covid",  className='menu-item'),
            ],
            vertical=True,
            pills=True  # add box around active menu item
        )], id='mySidebar', className='sidebar', style={'backgroundColor': '#DBE4EE'}),
    html.Div([], id="page-content", className="content")
])


# UPDATE VARIABLE PAGE
@app.callback(Output('param-view', 'children'),
              [Input('variable-picker', 'value')])
def update_map(selected_category):
    contents = get_variables_pagecontent(selected_category)
    return contents


# UPDATE REGRESSION PAGE
@app.callback(Output('regress-view', 'children'),
              [Input('regression-picker', 'value')])
def update_scatter(selected_category):
    contents = get_regression_pagecontent(selected_category)
    return contents


# UPDATE OUTPUT MODEL PAGE
@app.callback(Output('outp-view', 'children'),
              [Input('output-picker', 'value')])
def update_accepted_model_map(selected_outp):
    contents = get_output_pagecontent(selected_outp)
    return contents


#################
# ROUTING
@app.callback(
    [Output("variables", "active"), Output("regression", "active"), Output("home", "active"), Output(
        "model", "active"), Output("output", "active"), Output("covid", "active"), ],
    [Input("url", "pathname")],
)
def toggle_active_links(pathname):
    return [pathname == f"/{i}" for i in ("variables", "regression", "", "model", "output", "covid")]


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname in ["/variables"]:
        return variables
    elif pathname == "/regression":
        return regression
    elif pathname == "/":
        return homepage
    elif pathname == "/model":
        return model
    elif pathname == "/output":
        return output
    elif pathname == "/covid":
        return covid
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404", className="text-danger"),
            html.P("Congratulations, you discovered a page that does not exist"),
            html.Hr(),
            html.P(
                f"If you think pathname {pathname} should be in this app, contact Tijl.DeBie@ugent.be"),
        ]
    )


# HOMEPAGE BUTTON HANDLERS
@app.callback(
    Output("collapse-project", "is_open"),
    [Input("collapse-button-project", "n_clicks")],
    [State("collapse-project", "is_open")],
)
def toggle_collapse_project(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("collapse-tooling", "is_open"),
    [Input("collapse-button-tooling", "n_clicks")],
    [State("collapse-tooling", "is_open")],
)
def toggle_collapse_tooling(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("collapse-params", "is_open"),
    [Input("collapse-button-params", "n_clicks")],
    [State("collapse-params", "is_open")],
)
def toggle_collapse_params(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("collapse-findings", "is_open"),
    [Input("collapse-button-findings", "n_clicks")],
    [State("collapse-findings", "is_open")],
)
def toggle_collapse_findings(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("collapse-etl", "is_open"),
    [Input("collapse-button-etl", "n_clicks")],
    [State("collapse-etl", "is_open")],
)
def toggle_collapse_etl(n, is_open):
    if n:
        return not is_open
    return is_open


if __name__ == "__main__":
    app.run_server(port=8838, dev_tools_hot_reload=True, debug=True)
