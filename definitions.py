import os
import pandas as pd

ROOT_DIR = os.path.dirname(os.path.abspath(
    __file__))  # This is the Project Root
ASSET_PATH = os.path.join(ROOT_DIR, 'assets')

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)
