import json
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import os

# import sys
# sys.path.append('/Users/thibaultdegrande/Drive privé/Big_Data_Science/PROJECT/BDS_dash')  # set project root when editing in Visual Studio Code
from definitions import ASSET_PATH

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html


# Load data
df_model = pd.read_csv(os.path.join(
    ASSET_PATH, 'regression_accepted_model_data.csv'))
df_params = pd.read_csv(os.path.join(
    ASSET_PATH, 'regression_params_accepted_model.csv'))

explanation = dcc.Markdown(
    '''
    The following parameters have been accepted for the model:
    * population density (logarithmic)
    * total amount of houses
    * average amount of rooms
    * cars per 1000 inhabitants

    It can be seen that the predictions somewhat flatten out the mobiscores: low mobiscores are overestimated, high mobiscores are underestimated. This is also clear from the error graph on the right.

    '''
)


def get_accepted_model_stats():
    accepted_model_stats = dbc.Table.from_dataframe(df_params.round(
        4), striped=True, bordered=True, hover=True, size='sm')
    return accepted_model_stats


def predicted_real_scatter():
    # get scatter data
    real_values = list(df_model['avg(mobiscore)'].values)
    predicted_values = list(df_model['prediction'].values)
    # plot figure
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=real_values, y=predicted_values,
                             mode='markers'))  # name=str(selected_category),
    # fig.add_trace(go.Scatter(x=x_range, y=regression_line, mode='lines'))
    fig.update_xaxes(title_text='Real values')
    fig.update_yaxes(title_text='Predicted values')

    fig.add_shape(
        # Line Diagonal
            type="line",
            x0=0,
            y0=0,
            x1=11,
            y1=11,
            line=dict(
                color="Red",
                width=1
            )
    )

    fig.update_layout(showlegend=False, margin={"r": 100, "t": 0, "l": 0, "b": 0})
    
    return fig


def error_real_scatter():
    # get scatter data
    real_values = list(df_model['avg(mobiscore)'].values)
    error_values = list(df_model['error'].values)
    # plot figure
    error_fig = go.Figure()
    # name=str(selected_category),
    error_fig.add_trace(go.Scatter(
        x=real_values, y=error_values, mode='markers'))
    # fig.add_trace(go.Scatter(x=x_range, y=regression_line, mode='lines'))
    error_fig.update_xaxes(title_text='Real values')
    error_fig.update_yaxes(title_text='Error values')

    # Add shapes
    error_fig.add_shape(
            # Line Vertical
            dict(
                type="line",
                x0=5,
                y0=-6,
                x1=5,
                y1=6,
                line=dict(
                    color="Red",
                    width=1
                    )
                )
            )
    error_fig.add_shape(
        # Line Horizontal
            type="line",
            x0=0,
            y0=0,
            x1=11,
            y1=0,
            line=dict(
                color="Red",
                width=1
            )
    )

    error_fig.update_layout(showlegend=False, margin={"r": 100, "t": 0, "l": 0, "b": 0})
    
    return error_fig


def get_model_pagecontent():
    model_content = [
                # html.Hr(),
                # html.Span(id='explanation', children="Explanation and listing of the parameters here", style={'paddingTop': 50}),
                dbc.Row([
                    dbc.Col(html.Div(id='stats', children=get_accepted_model_stats(), style={'paddingTop': 20, "width": 500})),
                    dbc.Col(html.Div(children=explanation), style={'paddingTop': 50, 'float': 'left', "marginRight": 100})
                 ]),
                 dbc.Row([
                     dbc.Col(
                         html.Div(dcc.Graph(id='scatter', figure=predicted_real_scatter()), style={'paddingTop': 20})
                     ),
                     dbc.Col(
                        html.Div(dcc.Graph(id='errorscatter', figure=error_real_scatter()), style={'paddingTop': 20})
                     )                    
                 ])
        ]
    return model_content
