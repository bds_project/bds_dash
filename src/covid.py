import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from definitions import ASSET_PATH
import plotly.graph_objects as go
import json
import pandas as pd
import plotly.express as px
import os

import sys
# set project root when editing in Visual Studio Code
# sys.path.append('/Users/thibaultdegrande/Drive privé/Big_Data_Science/PROJECT/BDS_dash')


df_params = pd.read_csv(os.path.join(
    ASSET_PATH, 'regression_mobi_covid_muni_PARAMS.csv'))


df_model = pd.read_csv(os.path.join(
    ASSET_PATH, 'regression_mobi_covid_muni.csv'))
# df_model.rename(columns={'avg(avg(mobiscore))': 'avg(mobiscore)',
#                         'avg(covid_1k)': 'covid_1K'}, inplace=True)


explanation = dcc.Markdown(
    '''

    It is expected that the Mobiscore is related to the prevalence of Covid-cases. Higher accessibility is expected to induce more interaction and consequently, infection.
    Additionally, since the Mobiscore is higher in urban clusters where people live more closely together and more opportunities for infection arise, this indirect measurement of a cluster is expected to ensure a positive relationship.

    Conversely, the model finds a negative relationship between Covid prevalence and the Mobiscore. The reason why this is the case is not entirely clear. It should be noted, however, that many people work in urban clusters, while being housed outside of city centers. Consequently, cases will exist where transmission took place in the urban cluster, while the report of the case is done in a different area. It remains unclear what the exact mechanisms are that explain prevalence in and outside of accessible urban clusters.

    '''
)

labels = {
    'covid_1K': 'COVID-19 cases per 1000 inhabitants',
    'avg(mobiscore)': "Average of the Mobiscore in the statistical sector. "
}


def _get_expected_values_single(intercept, coeff, mini, maxi):
    '''
    Linear function to generate values of the regression (red) line
    '''
    vals = [intercept+coeff*i for i in range(mini, maxi)]
    return vals


def get_covid_regression():
    '''
    Returns the values of the regression line
    '''
    param_line = _get_expected_values_single(
        df_params[df_params["predictor"] ==
                  "intercept"]["coefficient"].values[0],
        df_params[df_params["predictor"] ==
                  "avg(mobiscore)"]["coefficient"].values[0],
        0, 11
    )
    return param_line


def get_scatter_values():
    '''
    Returns x and y values for the scatter plot based on a parameter name
    '''
    y_values = df_model['avg(mobiscore)'].values
    x_values = df_model['covid_1K'].values
    return x_values, y_values


def get_scatter():
    # get values to plot
    x_values, y_values = get_scatter_values()
    regression_line = get_covid_regression()
    x_range = [i for i in range(0, 11)]

    # make plot
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=x_values, y=y_values, mode='markers'))
    fig.add_trace(go.Scatter(x=x_range, y=regression_line, mode='lines'))
    fig.update_xaxes(title_text=labels["avg(mobiscore)"], range=[0, 10])
    fig.update_yaxes(
        title_text='Covid cases per 1K people. ')
    fig.update_layout(showlegend=False, margin={
                      "r": 100, "t": 0, "l": 0, "b": 0})

    # fig.show()

    return fig


def get_covid_page_content():
    content = html.Div([
        dbc.Row([
            dbc.Col(html.Span(id='covid_explanation', children=explanation, style={'paddingTop': 50}), width={"size": 9, "order": 1, "offset": 1}, style={"marginTop": "40px", "textAlign": "justify"})]),
        dbc.Row([
            dbc.Col(
                html.Div(dcc.Graph(id='covid_scatter', figure=get_scatter(), style={'paddingTop': 20, 'height': "350px"})), width={"size": 5, "order": 1, "offset": 1}
            ),
            dbc.Col(dbc.Table.from_dataframe(df_params.round(
                4), striped=True, bordered=True, hover=True, size='sm'),
                width={"size": 2, "order": 2},
                style={"marginTop": "90px"})
        ]
        )]
    )
    return content
