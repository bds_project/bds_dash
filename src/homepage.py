import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

about_project = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Project objectives", className="card-title"),
            html.P(
                "This project is the final assignment of Big Data Science (E018210A)"),

            dbc.Card(
                dbc.CardBody(
                    dcc.Markdown(
                        '''
                        *Goal*: formulate a research or business question related to the COVID-19 pandemic, and turn a solution into a small product.\n
                        In this project, we gathered data on the statistical sectors in Flanders, Belgium, and checked if there is a correlation between them as well as with the numbers on reported COVID cases per municipality.\n
                        In particular, we investigated the correlation between those data sources and the mobiscore.
                        '''
                    )), outline=False),



        ]
    )
)

about_etl = dbc.Card(
    dbc.CardBody(
        [
            html.H4("ETL-process", className="card-title"),
            html.P(
                "Information on the extraction and transformation of the input data"),
            dbc.Collapse(
                dbc.Card(
                    dbc.CardBody(
                        dcc.Markdown(
                            '''
                        ###### Extract
                        * Statiscal sectors: geojsons are available at [Statbel](https://statbel.fgov.be/en/open-data/statistical-sectors).
                        * Population, fiscal income: available as [open data](https://statbel.fgov.be/en/open-data?category=209).
                        * Car ownership: available per municipality at [Statistics Flanders](https://statistieken.vlaanderen.be/QvAJAXZfc/notoolbar.htm?document=SVR%2FSVR-Mobiliteit.qvw&host=QVS%40cwv100154&anonymous=true)
                        * Houses, rooms: data extracted from csv files available at [Census 2011](https://www.census2011.be/download/downloads_nl.html). Total amounts of houses, and the average amount of rooms were derived from this data.
                        * Mobiscores: for 10 randomly selected addresses per statistical sector, the [mobiscore](https://mobiscore.omgeving.vlaanderen.be/) and partial scores were scraped.
                        * COVID-cases: latest municipality covid data from [Sciensano](https://epistat.sciensano.be/Data/COVID19BE_CASES_MUNI_CUM.csv)
                        * Business addresses: all business addresses in Flanders, about 1.4M lines. [(KBO)](https://download.vlaanderen.be/Producten/Detail?id=3942&title=VKBO_ondernemingen_en_vestigingseenheden)

                        ###### Transform
                        * Population was combined with surface to get population density
                        * Car ownership and COVID-cases were assigned to the statistical sectors within the municipality
                        * Car ownership, COVID-cases and business addresses were combined with surface or population to normalise those parameters
                        * Business addresses: each business address was allocated to a statistical sector.
                        * Business addresses, mobiscores: summed, and averages per statistical sector

                        ###### Load
                        * Combine all statsec data
                        '''
                        )), outline=False),

                id="collapse-etl",
            ),
            dbc.Col([], width=6, style={"height": 10}),
            dbc.Button(
                "More",
                id="collapse-button-etl",
                className="mb-3",
                color="primary",
            )
        ]
    )
)


about_disclaimer = dbc.Card([
    dbc.CardImg(src="/assets/dash-logo.jpg", top=True),
    dbc.CardBody(
        [
            html.H5("Updating...", className="card-title"),
            html.P(dcc.Markdown(
                '''
                Due to the high amount of polygons that your browser has to render on the map, certain tabs can take up to 15 seconds to load.\n
                The visual is worth the wait!
                '''
            ))
        ]
    )
]
)


about_us = dbc.Card(
    dbc.CardBody(
        [
            html.H5("About us", className="card-title"),
            html.P(dcc.Markdown(
                '''
                Timo Latruwe\n
                Thibault Degrande
                '''
            ))
        ]
    )
)


about_statsecs = dbc.Card(
    dbc.CardBody(
        [
            html.H5("What are statistical sectors?", className="card-title"),
            html.P(dcc.Markdown(
                '''
                The statistical sector is the most detailed territorial level used by Statbel for its statistics and publications.\n
                The outlines of these sectors are available on their [open data portal](https://statbel.fgov.be/en/open-data?category=209).
                '''
            ))
        ]
    )
)


about_tooling = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Tooling", className="card-title"),
            html.P("This product was realized with different Big Data Tools"),

            dbc.Card(
                dbc.CardBody(
                    dcc.Markdown(
                        '''
                         * Web scraping: *Selenium*
                         * Data processing: *Pandas*, *PySpark Dataframes*
                         * Geospatial data processing: *Shapely*, *GeoPandas*
                         * Cloud hosting: *Amazon Web Services*
                         * Linear Regression: *PySpark MLlib*
                         * Data visualization: *Mapbox*, *Matplotlib*, *Plotly*, *Dash*
                        '''
                    )), outline=False),



        ]
    )
)


about_parameters = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Input data", className="card-title"),
            html.P(
                dcc.Markdown(
                    '''
                Several parameters were looked into, as listed below.
                More information can be found in the Variables-tab.\n
                Disclaimer: This tab might take some time to load!
                '''
                )
            ),

            dbc.Card(
                dbc.CardBody(
                    dcc.Markdown(
                        '''
                         * Car ownership
                         * House sizes
                         * Mobiscores
                         * COVID-cases
                         * Fiscal income
                         * Business addresses
                         * Population density
                        '''
                    )), outline=False),



        ]
    )
)


about_findings = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Outcomes", className="card-title"),
            html.P(
                dcc.Markdown(
                    '''
                We found that the following parameters are signifcantly correlated to the mobiscore.
                Further, COVID prevalence is found to be negatively correlated with the Mobiscore.
                '''
                )
            ),

            dbc.Card(
                dbc.CardBody(
                    dcc.Markdown(
                        '''
                         * Population density
                         * Total amount of houses
                         * The number of rooms
                         * Car ownership
                        '''
                    )), outline=False),
        ]
    )
)


homepage = html.Div([
    html.H3("What makes the Mobiscore?", style={
            'font': 'bold', 'paddingBottom': 30}),
    dbc.Row([
            dbc.Row(
                dbc.Col(about_project, width=12), style={
                    "marginTop": "15px"}),
            dbc.Row([
                dbc.Col(about_parameters, width=6),
                dbc.Col(about_tooling, width=6),
            ], style={"marginTop": "15px"})
            ], style={"marginTop": "15px", "padding": 15}),
    dbc.Row([
        dbc.Col(about_etl, width={"size": 12}, style={
            "marginTop": "15px", "padding": 15})]),
    dbc.Row([
        dbc.Col(about_findings, width=12, style={"marginTop": "15px"}), ]),
    dbc.Row([
        dbc.Col(about_statsecs, width=4, style={
                "marginTop": "15px"}),
        dbc.Col(about_us, width=4, style={"marginTop": "15px"}),
        dbc.Col(about_disclaimer, width=4, style={"marginTop": "15px"}),
    ]
    )

],
    style={'width': '100%', 'float': 'left', 'paddingTop': 50,
           'paddingLeft': 300, 'paddingRight': 100, 'marginBottom': 50}
)
