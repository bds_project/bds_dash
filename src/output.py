import json
import pandas as pd
import plotly.express as px
import os
import geopandas as gpd

# import sys
# sys.path.append('/Users/thibaultdegrande/Drive privé/Big_Data_Science/PROJECT/BDS_dash')  # set project root when editing in Visual Studio Code
from definitions import ASSET_PATH

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

statsecs = json.load(open(os.path.join(ASSET_PATH, 'statsec_clean.geojson')))

df = pd.read_csv(os.path.join(
    ASSET_PATH, 'regression_accepted_model_data.csv'))

labels = {
    'avg(mobiscore)': 'Average mobiscore',
    'prediction': 'Predicted Mobiscore',
    'error': 'Deviation from observed value: Observed - Described',
}

# define color-ranges (automatically too many errors because of outliers..)
start_end_ranges = {
    'avg(mobiscore)': {'start': 0, 'end': 10},
    'prediction': {'start': 0, 'end': 10},
    'error': {'start': -5, 'end': 5}
}

# set parameter descriptions
descriptions = {
    'avg(mobiscore)': """Actual average mobiscore per statistical sector. """,
    'prediction': """This chloropleth shows the expected Mobiscore of the sector according to the model. The variation in scores is lower than the observed numbers. """,
    'error': """The error term of the model indicates that it underestimates the height of Mobiscores in urban centers, and overestimates the Mobiscore in rural areas despite the inclusion of variables that should be decent proxies for it."""
}

# dropdown menu
output_options = []
df_categories = df.drop(columns=["pop_density_log", "total_houses", "avg_rooms", "cars_1K_fix", "statsec"
                                 ])
categories = list(df_categories.columns)

for category in categories:
    output_options.append(
        {'label': labels[category], 'value': category})


def get_fig(selected_category):
    start = start_end_ranges[selected_category]['start']
    end = start_end_ranges[selected_category]['end']
    colorscale = "viridis" if selected_category != "error" else "RdBu"
    fig = px.choropleth_mapbox(df, geojson=statsecs, color=selected_category,
                               locations="statsec",
                               featureidkey="properties.statsec",
                               color_continuous_scale=colorscale,
                               range_color=(start, end),
                               labels={labels[selected_category]: labels[selected_category],
                                       'name': labels[selected_category]},
                               center={"lat": 50.830098, "lon": 4.447480},
                               mapbox_style="carto-positron",
                               zoom=7
                               )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    return fig


def get_output_pagecontent(parameter):
    variables = [html.Span(id='explanation', children=descriptions[parameter], style={'paddingTop': 50}),
                 # html.Div(id='stats', children=get_stats(parameter),
                 #         style={'paddingTop': 20, 'width': 200}),
                 html.Div(dcc.Graph(id='map', figure=get_fig(parameter)), style={'paddingTop': 20})]

    return variables
