from definitions import ASSET_PATH
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go

import os
import pandas as pd

import sys
# set project root when editing in Visual Studio Code
sys.path.append(
    '/Users/thibaultdegrande/Drive privé/Big_Data_Science/PROJECT/BDS_dash')


# Load data
df_regr_params = pd.read_csv(os.path.join(
    ASSET_PATH, 'individual_regressions_params.csv'))
df_all_data = pd.read_csv(os.path.join(
    ASSET_PATH, 'individual_regression_input.csv'))


# labels for dropdown
labels = {
    'pop_density_log': 'population density (logarithmic)',
    'MS_AVG_TOT_NET_TAXABLE_INC': 'Average net income',
    'total_houses': 'Total amount of houses',
    'avg_rooms': 'Average amount of rooms',
    'cars_1K_fix': 'Vehicles per 1000 inhabitants',
    'covid_1K': 'COVID-19 cases per 1000 inhabitants',
    'KBO_km2': 'Business address density'
}

# text with explanation about regression results
regression_explanation = {
    'pop_density_log': """
    Since the population density seems to follow on exponential distribution, its natural logarithm is used as the explanatory variable. The population density of statistical sectors is expected to be the most important parameter explaining the Mobiscore for several reasons. First, dense population clusters usually contain a lot of the amenities that are included in the mobiscore. It makes sense for commercial and public organizations to be close to where most people are. Second, clusters of people also mean clusters of demand for public and mass transport, making investment in public transport options, such as trams, trains, and subways easier to justify.
    \nThe OLS regression indicates a significant and relatively strong explanatory power of the Mobiscore by the population density, though the assumptions of the model are not investigated further.
     """,
    'MS_AVG_TOT_NET_TAXABLE_INC': """
    The taxable income might affect the Mobiscore through several channels. It might be presumed that areas in which wealthier populations live have amenities closeby. Secondly, housing prices are often partially dependent on reachability, which means that the most reachable places are often more expensive than others and only wealthier people can afford to live there. On the other hand, the Mobiscore strongly depends on reachability by public transport, which is more often used by less wealthy people.
    \nThe regression shows a slightly negative correlation between the Mobiscore and the income. Perhaps this is caused by wealthy people living in greener and more open areas, which often require or induce transport by car.
    """,
    'total_houses': """
    The number of houses in an area can be a proxy for its rural character. We expect that the number of conventional houses is an identifier for clustering, which generally affects the Mobiscore in a positive way.
    \nThe regression shows a significant and relatively strong relationship between the number of buildings and the Mobiscore.
    """,
    'avg_rooms': """
    The average amount of rooms is expected to be a proxy for the rural character of a region, and thus to negatively correlate with the Mobiscore.
    \nThe regression show a string negative relationship with the Mobiscore, as can reasonably be expected.
    """,
    'cars_1K_fix': """
    The average amount of cars per inhabitant is expected to be related to the Mobiscore in several ways. First and foremost, a bidirectional relationship between car ownership and inferior reachability is expected to exist. Low accessibility liekly induces car ownership in people, while car ownership partially determines housing options, in the sense that convenience of a location is affected by prior ownership. Secondly, areas with high car ownership are in less need of access through other modes, which could reduces demand and supply for other options.
    \nThough figures are only available on the municipal level, a significant negative relationship can be discerned.
    """,
    'covid_1K': """
    On a municipal level, covid cases per inhabitant are available. It is expected that highly accessible areas have higher levels of interaction and consequently, more covid cases. Additionally, people living in close quarters, as is more often the case in urban clusters, are more likely to be infected.
    \nThe regression shows, however, that Mobiscore and the number of covid cases per capita are negatively correlated. No evident explanation presents itself.
    """,
    'KBO_km2': """
    The number of company addresses per squared km are expected to be postively correlated with the Mobiscore. Though the addresses do not necessarily indicate the location where work is performed, in many cases, they will. Since labour locations create a need for transport, accessibility to them is expected to be good. Additionally, and in a related manner, companies locate in accessible locations.
    \nThough there are some issues with the input data, a strong positive correlation is found.
    """
}

# dropdown menu
param_options = []
param_list = list(df_regr_params.predictor.values)

for param in param_list:
    param_options.append({'label': labels[param], 'value': param})


def _get_expected_values_single(intercept, coeff, mini, maxi):
    '''
    Linear function to generate values of the regression (red) line
    '''
    vals = [intercept+coeff*i for i in range(mini, maxi)]
    return vals


def get_individual_regression(parameter):
    '''
    Returns the values of the regression line based on a parameter name
    '''
    param_df = df_regr_params[df_regr_params.predictor == parameter]
    rnge = [param_df['min'].values[0], param_df['max'].values[0]]
    param_line = _get_expected_values_single(
        param_df['intercept'].values[0], param_df['coefficient'].values[0], param_df['min'].values[0], param_df['max'].values[0]+1)
    return param_line, rnge


def get_individual_scatter_values(parameter):
    '''
    Returns x and y values for the scatter plot based on a parameter name
    '''
    y_values = df_all_data['avg(mobiscore)'].values
    x_values = df_all_data[parameter].values
    return x_values, y_values


def get_scatter(selected_category):
    # get values to plot
    x_values, y_values = get_individual_scatter_values(selected_category)
    regression_line, rnge = get_individual_regression(selected_category)
    x_range = [i for i in range(rnge[0], rnge[1]+1)]

    # make plot
    fig = go.Figure()
    # name=str(selected_category),
    fig.add_trace(go.Scatter(x=x_values, y=y_values, mode='markers'))
    fig.add_trace(go.Scatter(x=x_range, y=regression_line, mode='lines'))
    fig.update_xaxes(title_text=labels[selected_category])
    fig.update_yaxes(title_text='Average mobiscore')
    fig.update_layout(showlegend=False, margin={
                      "r": 100, "t": 0, "l": 0, "b": 0})

    # fig.show()

    return fig


def get_explanation_results(parameter):
    explanation = regression_explanation[parameter]
    return explanation


def get_parameter_regression_stats(parameter):
    param_df = df_regr_params[df_regr_params.predictor == parameter]
    param_df = param_df.drop(columns=['predictor', 'min', 'max'], axis=1)
    regression_stats = dbc.Table.from_dataframe(param_df.round(
        4), striped=True, bordered=True, hover=True, size='sm')
    return regression_stats


def get_regression_pagecontent(parameter):
    regression = html.Div([html.Span(id='explanation', children=get_explanation_results(parameter), style={'paddingTop': 50}),
                           html.Div(id='stats', children=get_parameter_regression_stats(
                               parameter), style={'paddingTop': 20, 'width': 500}),
                           html.Div(dcc.Graph(id='scatter', figure=get_scatter(parameter)), style={'paddingTop': 20})],  style={"padding": 50})
    return regression
