import json
import pandas as pd
import plotly.express as px
import os
from definitions import ASSET_PATH

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

statsecs = json.load(open(os.path.join(ASSET_PATH, 'statsec_clean.geojson')))
df = pd.read_csv(os.path.join(ASSET_PATH, 'dataviz_master.csv'))

labels = {
    'avg_mobiscore': 'Average mobiscore',
    'pop_density(perkm2)': 'Population density',
    'KBO_1K': 'Business addresses',
    'cars_1K': 'Car ownership',
    'covid_1K': 'COVID-19 cases',
    'MS_AVG_TOT_NET_TAXABLE_INC': 'Average income',
    'MS_MEDIAN_NET_TAXABLE_INC': 'Median income',
    'avg_transit': 'Public Transport mobiscore',
    'avg_education': 'Education mobiscore',
    'avg_commercial': 'Commercial mobiscore',
    'avg_leisure': 'Leisure mobiscore',
    'avg_healthcare': 'Healthcare mobiscore',
    'avg_rooms': 'House sizes',
}

# define color-ranges (automatically too many errors because of outliers..)
start_end_ranges = {
    'avg_mobiscore': {'start': 0, 'end': 10},
    'pop_density(perkm2)': {'start': 0, 'end': 3000},
    'KBO_1K': {'start': 0, 'end': 500},
    'cars_1K': {'start': 300, 'end': 600},
    'covid_1K': {'start': 0, 'end': 10},
    'MS_AVG_TOT_NET_TAXABLE_INC': {'start': 0, 'end': 50000},
    'MS_MEDIAN_NET_TAXABLE_INC': {'start': 0, 'end': 40000},
    'avg_transit': {'start': 0, 'end': 5},
    'avg_education': {'start': 0, 'end': 5},
    'avg_commercial': {'start': 0, 'end': 5},
    'avg_leisure': {'start': 0, 'end': 5},
    'avg_healthcare': {'start': 0, 'end': 5},
    'avg_rooms': {'start': 4, 'end': 7},
}

# set parameter descriptions
descriptions = {
    'avg_mobiscore': 'Average mobiscore for 10 randomly picked addresses per statistical sector',
    'pop_density(perkm2)': 'Inhabitants per square kilometer',
    'KBO_1K': 'Amount of enterprises per 1000 inhabitants',
    'cars_1K': 'Amount of cars per 1000 inhabitants',
    'covid_1K': 'Amount of covid cases per 1000 inhabitants',
    'MS_AVG_TOT_NET_TAXABLE_INC': 'Average net taxable income per statistical sector',
    'MS_MEDIAN_NET_TAXABLE_INC': 'Median net taxable income per statistical sector',
    'avg_transit': 'Average partial score on Public Transport for 10 randomly picked addresses per statistical sector',
    'avg_education': 'Average partial score on Education for 10 randomly picked addresses per statistical sector',
    'avg_commercial': 'Average partial score on commercial points of interest for 10 randomly picked addresses per statistical sector',
    'avg_leisure': 'Average partial score on leisure activities for 10 randomly picked addresses per statistical sector',
    'avg_healthcare': 'Average partial score on healthcare places for 10 randomly picked addresses per statistical sector',
    'avg_rooms': 'Average amount of rooms in a house per statistical sector',
}

# sources:
sources = {
    'avg_mobiscore': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/) ''',
    'pop_density(perkm2)': '''Source: [StatBel](https://statbel.fgov.be/en/open-data?category=209)''',
    'KBO_1K': '''Source: [(KBO)](https://download.vlaanderen.be/Producten/Detail?id=3942&title=VKBO_ondernemingen_en_vestigingseenheden)''',
    'cars_1K': '''Source: [Statistics Flanders](https://statistieken.vlaanderen.be/QvAJAXZfc/notoolbar.htm?document=SVR%2FSVR-Mobiliteit.qvw&host=QVS%40cwv100154&anonymous=true)''',
    'covid_1K': '''Source: [Sciensano](https://epistat.sciensano.be/Data/COVID19BE_CASES_MUNI_CUM.csv)''',
    'MS_AVG_TOT_NET_TAXABLE_INC': '''Source: [StatBel](https://statbel.fgov.be/en/open-data?category=178)''',
    'MS_MEDIAN_NET_TAXABLE_INC': '''Source: [StatBel](https://statbel.fgov.be/en/open-data?category=178)''',
    'avg_transit': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/)''',
    'avg_education': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/)''',
    'avg_commercial': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/)''',
    'avg_leisure': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/)''',
    'avg_healthcare': '''Source: [mobiscore](https://mobiscore.omgeving.vlaanderen.be/)''',
    'avg_rooms': '''Source: [Census 2011](https://www.census2011.be/download/downloads_nl.html)''',
}


# dropdown menu
variable_options = []
df_categories = df.drop(
    columns=['statsec', 'statsec_name', 'municipality', 'province',
             'region', 'total_houses', 'MS_NBR_NON_ZERO_INC'],
    axis=1)
categories = list(df_categories.columns)

for category in categories:
    variable_options.append({'label': labels[category], 'value': category})


# stats on the parameter
def get_stats(parameter):
    stats = df[parameter].describe()
    idx = list(stats.index)
    # prevent table to contain many decimals
    values = [round(x, 2) for x in stats.values]

    stat_df = pd.DataFrame(
        {
            "Statistic": idx,
            "Value": values,
        }
    )

    stat_df = stat_df.set_index('Statistic')
    statstable = dbc.Table.from_dataframe(
        stat_df.transpose(), striped=True, bordered=True, hover=True, size='sm')
    return statstable


def get_fig(selected_category):
    start = start_end_ranges[selected_category]['start']
    end = start_end_ranges[selected_category]['end']

    fig = px.choropleth_mapbox(df, geojson=statsecs, color=selected_category,
                               locations="statsec",
                               featureidkey="properties.statsec",
                               color_continuous_scale="Viridis",
                               range_color=(start, end),
                               labels={labels[selected_category]: labels[selected_category],
                                       'name': labels[selected_category]},
                               center={"lat": 50.830098, "lon": 4.447480},
                               mapbox_style="carto-positron",
                               zoom=7
                               )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    return fig


def get_variables_pagecontent(parameter):
    variables = [
        html.Span(id='explanation', children=descriptions[parameter], style={
                  'paddingTop': 50}),
        html.Span(dcc.Markdown(sources[parameter])),
        html.Div(id='stats', children=get_stats(parameter),
                 style={'paddingTop': 20, 'width': 200}),
        html.Div(dcc.Graph(id='map', figure=get_fig(
            parameter)), style={'paddingTop': 20})
    ]

    return variables
